/****************************************************************/
/*
Amirhossein Saririghavee
CSCI0046
Professor Barry Brown
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum password 
const int HASH_LEN=33;        // Maximum hash 
// Structure to hold both a plaintext password and a hash.

struct entry 
{
   char pass[40];
   char hash[40];
};
/****************************************************************/
struct entry *read_dictionary(char *filename, int *size)
{
   int arraylength=10;
   *size = 0;
   
   //open dictionary file 
   FILE *indictionary = fopen(filename, "r");
   
   //check for error
   if (!indictionary)
   {
      perror("Can't open dictionary");
      exit(1);
   }  
    
   // Allocate memory for struct.
   struct entry *s=malloc(arraylength*sizeof(struct entry));
   int entries=0;
   char mypass[PASS_LEN]="";
   char myhash[HASH_LEN]="";    
  
   // Read the dictionary line by line.
   while(fgets(mypass,50,indictionary) != NULL)
   {  
      //exchange \n with \0 if exists 
      char *nl1 = strchr(mypass, '\n');
      if(nl1 != NULL)
         *nl1 = '\0';
      //make hash from the string just read   
      char *ihash = md5(mypass, strlen(mypass));
      strcpy(myhash,ihash);
     
      // Expand array if necessary (realloc).
      if(entries==arraylength)
      {
         arraylength+= (arraylength* .25);
         s = realloc(s,arraylength*sizeof(struct entry));
      }
      //copy password and hash to struct
      strcpy(s[entries].pass,mypass); 
      strcpy(s[entries].hash,myhash); 
      entries++;
   }
   //copy number of entries to size pointer so that we could use in main
   *size = entries;
   fclose (indictionary);
   return s;
}
/****************************************************************/


/****************************************************************/
//function for quick sort
int byhash(const void *a, const void *b)
{
   struct entry *aa = (struct entry *)a;
   struct entry *bb = (struct entry *)b;
   return strcmp(aa->hash, bb->hash);
// or return strcmp((*aa).hash, (*bb).hash);
}
/****************************************************************/


/****************************************************************/
//fuction for binary search
int bcomp(const void *t,const void *elem)
{
   char *tt = (char *)t;
   struct entry *eelem = (struct entry *)elem; 
   return strcmp(t, eelem->hash);
}
/****************************************************************/


/****************************************************************/
//main 
int main(int argc, char *argv[])
{
   int dlen;
   if (argc < 3) 
   {
      printf("Usage: %s hash_file dict_file\n", argv[0]);
      exit(1);
   }
   //Read the dictionary file into an array of entry structures
   struct entry *dict = read_dictionary(argv[1],&dlen);
   
   // Sort the hashed dictionary using qsort.
   qsort(dict,dlen,sizeof(struct entry),byhash);
   
   char hashread[HASH_LEN+1]="";
     // Open the hash file for reading.
   FILE *readhash = fopen(argv[2], "r");
   if (!readhash)
   {
      perror("Can't open dictionary");
      exit(1);
   }
   //read sample hash file and find the corresponding word
   while(fgets(hashread,50,readhash) != NULL)
   { 
      char *nl1 = strchr(hashread, '\n');
      if(nl1 != NULL)
         *nl1 = '\0'; 
         
      // For each hash, search for it in the dictionary using binary search.
      struct entry *found = bsearch(hashread,dict,dlen, sizeof(struct entry), bcomp);
      
      // If you find it, get the corresponding plaintext dictionary word.
      if (found)
      {
         printf("hash: %s  password is: %s\n" ,(*found).hash,(*found).pass);
      }
      else
      {
         printf("hash: %s Not found. Awwww.\n",hashread);
      }
   }
    fclose (readhash);
    free(dict);
}
/****************************************************************/